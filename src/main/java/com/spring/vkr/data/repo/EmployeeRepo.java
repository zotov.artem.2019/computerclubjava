package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.Client;
import com.spring.vkr.data.entites.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("from Employee " +
            "where lower(name) like lower(concat('%', :searchTerm,'%')) " +
            "or lower(surname) like lower(concat('%', :searchTerm,'%')) " +
            "or lower(patronymic) like lower(concat('%', :searchTerm,'%')) ")
    List<Employee> search(@Param("searchTerm") String searchTerm);
}

