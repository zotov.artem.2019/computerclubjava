package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.Computer;
import com.spring.vkr.data.entites.ComputerType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ComputerRepo extends JpaRepository<Computer, Long> {
    @Query("from Computer " +
            "where id like concat('%', :searchTerm,'%') " +
            "or type like concat('%', :searchTerm,'%')")
    List<Computer> search(@Param("searchTerm") String searchTerm);
}
