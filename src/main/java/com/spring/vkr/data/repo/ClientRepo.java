package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepo extends JpaRepository<Client, Long> {
    @Query("from Client " +
            "where lower(name) like lower(concat('%', :searchTerm,'%')) " +
            "or lower(surname) like lower(concat('%', :searchTerm,'%')) " +
            "or lower(patronymic) like lower(concat('%', :searchTerm,'%')) ")
    List<Client> search(@Param("searchTerm") String searchTerm);
}
