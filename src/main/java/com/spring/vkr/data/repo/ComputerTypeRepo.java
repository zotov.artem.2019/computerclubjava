package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.ComputerType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ComputerTypeRepo extends JpaRepository<ComputerType, Long> {
    @Query("from ComputerType " +
            "where id like concat('%', :searchTerm,'%') " +
            "or name like concat('%', :searchTerm,'%')")
    List<ComputerType> search(@Param("searchTerm") String searchTerm);
}
