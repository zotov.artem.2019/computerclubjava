package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.CardType;
import com.spring.vkr.data.entites.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CardTypeRepo extends JpaRepository<CardType, Long> {

    @Query("from CardType " +
            "where lower(name) like lower(concat('%', :searchTerm,'%')) " +
            "or id like concat('%', :searchTerm,'%')")
    List<CardType> search(@Param("searchTerm") String searchTerm);
}
