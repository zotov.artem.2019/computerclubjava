package com.spring.vkr.data.repo;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CardRepo extends JpaRepository<Card, Long> {
   @Query("from Card " +
            "where id like concat('%', :searchTerm,'%') " +
            "or type like concat('%', :searchTerm,'%')")
    List<Card> search(@Param("searchTerm") int searchTerm);
}
