package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "computer")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comp_id")
    private Long id;
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "type")
    private ComputerType type;
    @NotEmpty
    private String os;
    @NotEmpty
    private String keyboard;
    @NotEmpty
    private String mouse;
    @NotEmpty
    private String monitor;
    @NotEmpty
    private String headset;
    @ManyToMany
    @JoinTable(name = "rented_computer",
            joinColumns = @JoinColumn(name = "computer"),
            inverseJoinColumns = @JoinColumn(name = "rent"))
    private List<Rent> rents;

}
