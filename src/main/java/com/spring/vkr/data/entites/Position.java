package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "position")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pos_id")
    private Long id;

    private String name;

    private float salary;
    @OneToMany(mappedBy = "position", fetch = FetchType.EAGER)
    private List<Employee> employees;

}
