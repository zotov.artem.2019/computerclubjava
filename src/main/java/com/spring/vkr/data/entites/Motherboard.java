package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "motherboard")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Motherboard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "motherboard_id")
    private Long id;
    @NotEmpty
    private String vendor;
    @NotEmpty
    private String model;

    @OneToMany(mappedBy = "motherboard", fetch = FetchType.EAGER)
    private List<Components> sets;
}