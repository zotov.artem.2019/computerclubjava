package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "set_of_components")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Components {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "set_id")
    private Long id;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "gpu")
    private Gpu gpu;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "cpu")
    private Cpu cpu;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "motherboard")
    private Motherboard motherboard;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "ram")
    private Ram ram;

    @OneToOne(mappedBy = "components", optional = false)
    private ComputerType type;

}
