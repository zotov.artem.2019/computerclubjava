package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "card_type")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CardType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private double discount;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    private List<Card> cards;
}
