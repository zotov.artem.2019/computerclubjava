package com.spring.vkr.data.entites;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "employee")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employee_id")
    private Long id;
    @NotEmpty
    private String surname;
    @NotEmpty
    private String name;
    private String patronymic;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "position")
    private Position position;
    @NotEmpty
    private String login;
    @NotEmpty
    private String password;
    @NotEmpty
    private String passportSeries;
    @NotEmpty
    private String passportId;
    private String address;
    @NotEmpty
    private String phone;
    private LocalDate dateOfBirth;
    private LocalDate hireDate;

    public String getFullName() {
        return surname.concat(" ").concat(name).concat(" ").concat(patronymic);
    }

    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private List<Rent> rents;

}
