package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "computer_type")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ComputerType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private Long id;

    private String name;

    private double costPerHour;

    @OneToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "components")
    private Components components;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    private List<Computer> computers;
}
