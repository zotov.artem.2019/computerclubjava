package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "gpu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Gpu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gpu_id")
    private Long id;

    @NotEmpty
    private String gpu;
    @NotEmpty
    private String vendor;
    @NotEmpty
    private String model;

    @OneToMany(mappedBy = "gpu", fetch = FetchType.EAGER)
    private List<Components> sets;
}
