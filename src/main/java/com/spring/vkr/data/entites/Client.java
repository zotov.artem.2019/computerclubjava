package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "client")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private Long id;
    @NotEmpty
    private String surname;
    @NotEmpty
    private String name;
    private String patronymic;
    @NotEmpty
    private String login;
    @NotEmpty
    private String password;
    @NotEmpty
    private String passportSeries;
    @NotEmpty
    private String passportId;
    private String address;
    @NotEmpty
    private String phone;

    private LocalDate dateOfBirth;

    @OneToOne(mappedBy = "client", optional = false)
    private Card card;

    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private List<Rent> rents;

    public String getFullName() {
        return surname.concat(" ").concat(name).concat(" ").concat(patronymic);
    }

}
