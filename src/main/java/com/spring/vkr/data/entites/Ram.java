package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "ram")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ram_id")
    private Long id;
    @NotEmpty
    private String volume;
    @NotEmpty
    private String vendor;
    @NotEmpty
    private String model;

    @OneToMany(mappedBy = "ram", fetch = FetchType.EAGER)
    private List<Components> sets;
}
