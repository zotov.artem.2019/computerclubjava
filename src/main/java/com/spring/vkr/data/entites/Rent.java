package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "rent")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rent_id")
    private Long id;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "client")
    private Client client;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "employee")
    private Employee employee;
    private LocalDateTime startDateAndTime;
    private Time duration;

    @ManyToMany
    @JoinTable(name = "rented_computer",
    joinColumns = @JoinColumn(name = "rent"),
    inverseJoinColumns = @JoinColumn(name = "computer"))
    private List<Computer> computers;

}
