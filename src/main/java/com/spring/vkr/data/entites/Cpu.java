package com.spring.vkr.data.entites;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "cpu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cpu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cpu_id")
    private Long id;
    @NotEmpty
    private String vendor;
    @NotEmpty
    private String model;

    @OneToMany(mappedBy = "cpu", fetch = FetchType.EAGER)
    private List<Components> sets;
}
