package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.ComputerType;
import com.spring.vkr.data.repo.CardRepo;
import com.spring.vkr.data.repo.ComputerTypeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComputerTypeService {

    private final ComputerTypeRepo computerTypeRepo;

    public ComputerTypeService(ComputerTypeRepo computerTypeRepo) {
        this.computerTypeRepo = computerTypeRepo;
    }

    public List<ComputerType> findAllComputerTypes(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return computerTypeRepo.findAll();
        } else {
            return computerTypeRepo.search(stringFilter);
        }
    }

    public long countComputerTypes() {
        return computerTypeRepo.count();
    }

    public void deleteComputerType(ComputerType computerType) {
        computerTypeRepo.delete(computerType);
    }

    public void saveComputerType(ComputerType computerType) {
        if (computerType == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        computerTypeRepo.save(computerType);
    }

    public List<ComputerType> findAllComputerTypes() {
        return computerTypeRepo.findAll();
    }

}
