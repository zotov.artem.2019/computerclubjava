package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Computer;
import com.spring.vkr.data.repo.ComputerRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComputerService {
    private final ComputerRepo computerRepo;

    public ComputerService(ComputerRepo computerRepo) {
        this.computerRepo = computerRepo;
    }

    public List<Computer> findAllComputers(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return computerRepo.findAll();
        } else {
            return computerRepo.search(stringFilter);
        }
    }

    public long countComputers() {
        return computerRepo.count();
    }

    public void deleteComputer(Computer computer) {
        computerRepo.delete(computer);
    }

    public void saveComputer(Computer computer) {
        if (computer == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        computerRepo.save(computer);
    }

    public List<Computer> findAllComputers() {
        return computerRepo.findAll();
    }
}
