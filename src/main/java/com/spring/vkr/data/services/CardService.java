package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.Client;
import com.spring.vkr.data.repo.CardRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {

    private final CardRepo cardRepo;

    public CardService(CardRepo cardRepo) {
        this.cardRepo = cardRepo;
    }

    public List<Card> findAllCards(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return cardRepo.findAll();
        } else {
            return cardRepo.search(Integer.parseInt(stringFilter));
        }
    }

    public long countCards() {
        return cardRepo.count();
    }

    public void deleteCard(Card card) {
        cardRepo.delete(card);
    }

    public void saveCard(Card card) {
        if (card == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        cardRepo.save(card);
    }

    public List<Card> findAllCards() {
        return cardRepo.findAll();
    }
}
