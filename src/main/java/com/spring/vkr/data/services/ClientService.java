package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Client;
import com.spring.vkr.data.repo.ClientRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    private final ClientRepo clientRepo;

    public ClientService(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    public List<Client> findAllClients(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return clientRepo.findAll();
        } else {
            return clientRepo.search(stringFilter);
        }
    }

    public long countClients() {
        return clientRepo.count();
    }

    public void deleteClient(Client client) {
        clientRepo.delete(client);
    }

    public void saveClient(Client client) {
        if (client == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        clientRepo.save(client);
    }

    public List<Client> findAllClients() {
        return clientRepo.findAll();
    }

}
