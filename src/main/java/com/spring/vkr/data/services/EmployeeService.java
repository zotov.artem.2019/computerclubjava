package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Client;
import com.spring.vkr.data.entites.Employee;
import com.spring.vkr.data.repo.EmployeeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepo employeeRepo;

    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public List<Employee> findAllEmployees(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return employeeRepo.findAll();
        } else {
            return employeeRepo.search(stringFilter);
        }
    }

    public long countEmployee() {
        return employeeRepo.count();
    }

    public void deleteEmployee(Employee employee) {
        employeeRepo.delete(employee);
    }

    public void saveEmployee(Employee employee) {
        if (employee == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        employeeRepo.save(employee);
    }

    public List<Employee> findAllEmployees() {
        return employeeRepo.findAll();
    }
}
