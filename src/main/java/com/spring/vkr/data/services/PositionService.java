package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.Position;
import com.spring.vkr.data.repo.CardRepo;
import com.spring.vkr.data.repo.PositionRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionService {
    private final PositionRepo positionRepo;

    public PositionService(PositionRepo positionRepo) {
        this.positionRepo = positionRepo;
    }

    public List<Position> findAllPositions(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return positionRepo.findAll();
        } else {
            return positionRepo.search(stringFilter);
        }
    }

    public long countPositions() {
        return positionRepo.count();
    }

    public void deletePosition(Position position) {
        positionRepo.delete(position);
    }

    public void savePosition(Position position) {
        if (position == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        positionRepo.save(position);
    }

    public List<Position> findAllPositions() {
        return positionRepo.findAll();
    }
}
