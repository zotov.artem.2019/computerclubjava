package com.spring.vkr.data.services;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.CardType;
import com.spring.vkr.data.repo.CardRepo;
import com.spring.vkr.data.repo.CardTypeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardTypeService {
    private final CardTypeRepo typeRepo;

    public CardTypeService(CardTypeRepo typeRepo) {
        this.typeRepo = typeRepo;
    }

    public List<CardType> findAllCardTypes(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return typeRepo.findAll();
        } else {
            return typeRepo.search(stringFilter);
        }
    }

    public long countCardTypes() {
        return typeRepo.count();
    }

    public void deleteCardType(CardType type) {
        typeRepo.delete(type);
    }

    public void saveCardType(CardType type) {
        if (type == null) {
            System.err.println("Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        typeRepo.save(type);
    }

    public List<CardType> findAllCardTypes() {
        return typeRepo.findAll();
    }
}
