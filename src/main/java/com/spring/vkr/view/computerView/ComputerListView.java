package com.spring.vkr.view.computerView;

import com.spring.vkr.data.entites.Computer;
import com.spring.vkr.data.services.ComputerService;
import com.spring.vkr.data.services.ComputerTypeService;
import com.spring.vkr.view.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.util.stream.Stream;

@Route(value = "computers", layout = MainLayout.class)
@PageTitle("Computers | Computer club CRM")
public class ComputerListView extends VerticalLayout {
    private final Grid<Computer> grid = new Grid<>(Computer.class);
    private final TextField filterText = new TextField();

    private final ComputerTypeService compTypeService;

    private final ComputerService computerService;


    ComputerForm form;

    public ComputerListView(ComputerTypeService compTypeService, ComputerService computerServiceService) {
        this.compTypeService = compTypeService;
        this.computerService = computerServiceService;

        addClassName("computers-list-view");
        setSizeFull();
        configureGrid();
        configureForm();
        add(getToolbar(), getContent());
        updateList();
        closeEditor();
    }

    private static ComponentRenderer<ComputerDetailsFormLayout, Computer> createComputerDetailsRenderer() {
        return new ComponentRenderer<>(ComputerDetailsFormLayout::new,
                ComputerDetailsFormLayout::setComputer);
    }

    private void closeEditor() {
        form.setComputer(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    public void editComputer(Computer computer) {
        if (computer == null) {
            closeEditor();
        } else {
            form.setComputer(computer);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void addComputer() {
        grid.asSingleSelect().clear();
        editComputer(new Computer());
    }


    private void updateList() {
        grid.setItems(computerService.findAllComputers(filterText.getValue()));
    }

    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, form);
        content.setFlexGrow(2, grid);
        content.setFlexGrow(1, form);
        content.addClassName("content");
        content.setSizeFull();
        return content;

    }

    private void configureForm() {
        form = new ComputerForm(compTypeService.findAllComputerTypes());
        form.setWidth("25em");
        form.addSaveListener(this::saveComputer);
        form.addDeleteListener(this::deleteComputer);
        form.addCloseListener(e -> closeEditor());
    }

    private void saveComputer(ComputerForm.SaveEvent event) {
        computerService.saveComputer(event.getComputer());
        updateList();
        closeEditor();
    }

    private void deleteComputer(ComputerForm.DeleteEvent event) {
        computerService.deleteComputer(event.getComputer());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("computers-grid");
        grid.setSizeFull();
        grid.removeAllColumns();
        grid.addColumn("id").setHeader("ID");
        grid.addColumn(computer -> computer.getType().getName()).setHeader("Тип");
        grid.addColumn("os").setHeader("ОС");
        grid.addColumn("keyboard").setHeader("Клавиатура");
        grid.addColumn("mouse").setHeader("Мышь");
        grid.addColumn("monitor").setHeader("Монитор");
        grid.addColumn("headset").setHeader("Наушники");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));
        grid.setItemDetailsRenderer(createComputerDetailsRenderer());
        grid.asSingleSelect().addValueChangeListener(event ->
                editComputer(event.getValue()));
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Поиск");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(event -> {
            updateList();
        });

        Button addComputerButton = new Button("Добавить компьютер");
        addComputerButton.addClickListener(click -> addComputer());
        var toolbar = new HorizontalLayout(filterText, addComputerButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

    private static class ComputerDetailsFormLayout extends FormLayout {
        private final TextField gpu = new TextField("Видеокарта");
        private final TextField cpu = new TextField("Процессор");
        private final TextField motherboard = new TextField("Материнская плата");
        private final TextField ram = new TextField("Оперативная память");

        public  ComputerDetailsFormLayout() {
            Stream.of(gpu, cpu, motherboard, ram).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 2));

        }

        public void setComputer(Computer computer) {
            gpu.setValue(computer.getType().getComponents().getGpu().getVendor()
                            .concat(" ")
                    .concat(computer.getType().getComponents().getGpu().getGpu()
                            .concat(" "))
                    .concat(computer.getType().getComponents().getGpu().getModel()));
            cpu.setValue(computer.getType().getComponents().getCpu().getVendor()
                    .concat(" ")
                    .concat(computer.getType().getComponents().getCpu().getModel()));
            motherboard.setValue(computer.getType().getComponents().getMotherboard().getVendor()
                    .concat(" ")
                    .concat(computer.getType().getComponents().getMotherboard().getModel()));
            ram.setValue(computer.getType().getComponents().getRam().getVendor()
                    .concat(" ")
                    .concat(computer.getType().getComponents().getRam().getVendor()
                            .concat(" "))
                    .concat(computer.getType().getComponents().getRam().getVolume()));

        }
    }
}
