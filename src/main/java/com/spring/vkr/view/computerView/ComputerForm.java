package com.spring.vkr.view.computerView;

import com.spring.vkr.data.entites.*;
import com.spring.vkr.view.cardsView.CardForm;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

import java.util.List;

import static com.vaadin.flow.component.ComponentUtil.addListener;
import static com.vaadin.flow.component.ComponentUtil.fireEvent;
import static org.springframework.cglib.core.TypeUtils.add;

public class ComputerForm extends FormLayout {
    TextField os = new TextField("ОС");
    ComboBox<ComputerType> type = new ComboBox<>("Тип");
    TextField keyboard = new TextField("Клавиатура");
    TextField mouse = new TextField("Мышь");
    TextField monitor = new TextField("Монитор");
    TextField headset = new TextField("Наушники");
    Binder<Computer> binder = new BeanValidationBinder<>(Computer.class);
    Button save = new Button("Сохранить");
    Button delete = new Button("Удалить");
    Button cancel = new Button("Отмена");

    public ComputerForm(List<ComputerType> types) {
        initComboBox(types);
        binder.bindInstanceFields(this);
        addClassName("computer-form");
        add(
                type,
                os,
                keyboard,
                mouse,
                monitor,
                headset,
                createButtonLayout());
    }

    private void initComboBox(List<ComputerType> types) {
        type.setItems(types);
        type.setItemLabelGenerator(ComputerType::getName);
    }

    public void setComputer(Computer computer) {
        binder.setBean(computer);
    }

    private Component createButtonLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickShortcut(Key.ENTER);
        cancel.addClickShortcut(Key.ESCAPE);
        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new ComputerForm.DeleteEvent(this, binder.getBean())));
        cancel.addClickListener(event -> fireEvent(new ComputerForm.CloseEvent(this)));

        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, delete, cancel);
    }

    private void validateAndSave() {
        if (binder.isValid()) {
            fireEvent(new ComputerForm.SaveEvent(this, binder.getBean()));
        }
    }

    public static abstract class ComputerFormEvent extends ComponentEvent<ComputerForm> {
        private final Computer computer;

        protected ComputerFormEvent(ComputerForm source, Computer computer) {
            super(source, false);
            this.computer = computer;
        }

        public Computer getComputer() {
            return computer;
        }
    }

    public static class SaveEvent extends ComputerForm.ComputerFormEvent {
        SaveEvent(ComputerForm source, Computer computer) {
            super(source, computer);
        }
    }

    public static class DeleteEvent extends ComputerForm.ComputerFormEvent {
        DeleteEvent(ComputerForm source, Computer computer) {
            super(source, computer);
        }

    }

    public static class CloseEvent extends ComputerForm.ComputerFormEvent {
        CloseEvent(ComputerForm source) {
            super(source, null);
        }

    }

    public Registration addDeleteListener(ComponentEventListener<ComputerForm.DeleteEvent> listener) {
        return addListener(ComputerForm.DeleteEvent.class, listener);
    }

    public Registration addSaveListener(ComponentEventListener<ComputerForm.SaveEvent> listener) {
        return addListener(ComputerForm.SaveEvent.class, listener);
    }

    public Registration addCloseListener(ComponentEventListener<ComputerForm.CloseEvent> listener) {
        return addListener(ComputerForm.CloseEvent.class, listener);
    }

}
