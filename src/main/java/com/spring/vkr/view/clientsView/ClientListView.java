package com.spring.vkr.view.clientsView;

import com.spring.vkr.data.entites.Client;
import com.spring.vkr.data.services.ClientService;
import com.spring.vkr.view.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "clients", layout = MainLayout.class)
@PageTitle("Clients | Computer club CRM")
public class ClientListView extends VerticalLayout {
    Grid<Client> grid = new Grid<>(Client.class);
    TextField filterText = new TextField();

    ClientForm form;
    private final ClientService service;

    public ClientListView(ClientService service) {
        this.service = service;
        addClassName("clients-list-view");
        setSizeFull();
        configureGrid();
        configureForm();
        add(getToolbar(), getContent());
        updateList();
        closeEditor();
    }

    private void closeEditor() {
        form.setClient(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    public void editClient(Client client) {
        if (client == null) {
            closeEditor();
        } else {
            form.setClient(client);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void addClient() {
        grid.asSingleSelect().clear();
        editClient(new Client());
    }


    private void updateList() {
        grid.setItems(service.findAllClients(filterText.getValue()));
    }

    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, form);
        content.setFlexGrow(2,grid);
        content.setFlexGrow(1, form);
        content.addClassName("content");
        content.setSizeFull();
        return content;

    }

    private void configureForm() {
        form = new ClientForm();
        form.setWidth("25em");
        form.addSaveListener(this::saveClient);
        form.addDeleteListener(this::deleteClient);
        form.addCloseListener(e -> closeEditor());
    }

    private void saveClient(ClientForm.SaveEvent event) {
        service.saveClient(event.getClient());
        updateList();
        closeEditor();
    }

    private void deleteClient(ClientForm.DeleteEvent event) {
        service.deleteClient(event.getClient());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("clients-grid");
        grid.setSizeFull();
        grid.setColumns("id", "surname", "name", "patronymic","login", "password", "passportSeries",
                "passportId", "address","phone", "dateOfBirth");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));
        grid.asSingleSelect().addValueChangeListener(event ->
                editClient(event.getValue()));
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Поиск");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(event -> {updateList();});

        Button addClientButton = new Button("Add client");
        addClientButton.addClickListener(click -> addClient());
        var toolbar = new HorizontalLayout(filterText, addClientButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }
}