package com.spring.vkr.view.clientsView;

import com.spring.vkr.data.entites.Client;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

public class ClientForm extends FormLayout {
    public static final String PHONE_PATTERN = "[+][7]\\s[(][0-9][0-9][0-9][)]\\s[0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]";
    TextField surname = new TextField("Фамилия");
    TextField name = new TextField("Имя");
    TextField patronymic = new TextField("Отчество");
    TextField login = new TextField("Логин");
    TextField password = new TextField("Пароль");
    TextField passportSeries = new TextField("Серия паспорта");
    TextField passportId = new TextField("Номер паспорта");
    TextField address = new TextField("Адрес");
    TextField phone = new TextField("Телефон");
    DatePicker dateOfBirth = new DatePicker("Дата рождения");

    Button save = new Button("Сохранить");
    Button delete = new Button("Удалить");
    Button cancel = new Button("Отмена");

    Binder<Client> binder = new BeanValidationBinder<>(Client.class);

    DatePicker.DatePickerI18n singleFormatI18n = new DatePicker.DatePickerI18n();
    public ClientForm() {
        passportValidate();
        phoneValidate();
        datePickerValidate();
        binder.bindInstanceFields(this);
        addClassName("client-form");
        add(
                surname,
                name,
                patronymic,
                login,
                password,
                passportSeries,
                passportId,
                address,
                phone,
                dateOfBirth,
                createButtonLayout());
    }

    private void passportValidate() {
        passportSeries.setMinLength(4);
        passportSeries.setMaxLength(4);
        passportSeries.setAllowedCharPattern("[0-9]");
        passportId.setMinLength(6);
        passportId.setMaxLength(6);
        passportId.setAllowedCharPattern("[0-9]");
    }

    private void datePickerValidate() {
        singleFormatI18n.setDateFormat("yyyy-MM-dd");
        dateOfBirth.setI18n(singleFormatI18n);
    }

    private void phoneValidate() {
        phone.setRequiredIndicatorVisible(true);
        phone.setPattern(PHONE_PATTERN);
        phone.setAllowedCharPattern("[0-9()+- ]");
        phone.setMinLength(18);
        phone.setMaxLength(18);
        phone.setHelperText("Формат: +7 (123) 456-78-90");
    }

    public void setClient(Client client) {
        binder.setBean(client);
    }

    private Component createButtonLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickShortcut(Key.ENTER);
        cancel.addClickShortcut(Key.ESCAPE);
        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, binder.getBean())));
        cancel.addClickListener(event -> fireEvent(new CloseEvent(this)));

        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, delete, cancel);
    }

    private void validateAndSave() {
        if(binder.isValid()) {
            fireEvent(new SaveEvent(this, binder.getBean()));
        }
    }

    public static abstract class ClientFormEvent extends ComponentEvent<ClientForm> {
        private final Client client;

        protected ClientFormEvent(ClientForm source, Client client) {
            super(source, false);
            this.client = client;
        }

        public Client getClient() {
            return client;
        }
    }

    public static class SaveEvent extends ClientFormEvent {
        SaveEvent(ClientForm source, Client client) {
            super(source, client);
        }
    }

    public static class DeleteEvent extends ClientFormEvent {
        DeleteEvent(ClientForm source, Client client) {
            super(source, client);
        }

    }

    public static class CloseEvent extends ClientFormEvent {
        CloseEvent(ClientForm source) {
            super(source, null);
        }
    }

    public Registration addDeleteListener(ComponentEventListener<DeleteEvent> listener) {
        return addListener(DeleteEvent.class, listener);
    }

    public Registration addSaveListener(ComponentEventListener<SaveEvent> listener) {
        return addListener(SaveEvent.class, listener);
    }
    public Registration addCloseListener(ComponentEventListener<CloseEvent> listener) {
        return addListener(CloseEvent.class, listener);
    }
}
