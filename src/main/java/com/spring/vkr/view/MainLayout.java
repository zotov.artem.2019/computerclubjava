package com.spring.vkr.view;

import com.spring.vkr.view.cardsView.CardListView;
import com.spring.vkr.view.clientsView.ClientListView;
import com.spring.vkr.view.computerView.ComputerListView;
import com.spring.vkr.view.employeesView.EmployeeForm;
import com.spring.vkr.view.employeesView.EmployeeListView;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class MainLayout extends AppLayout {

    private final Button logoutButton = new Button("Log out");
    public MainLayout() {
        createHeader();
        createDrawer();
    }

    private void createHeader() {
        H1 logo = new H1("GG");
        logo.addClassNames(
                LumoUtility.FontSize.LARGE,
                LumoUtility.Margin.MEDIUM);

        logout();
        var header = new HorizontalLayout(new DrawerToggle(), logo, logoutButton);
        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setWidthFull();
        header.addClassNames(
                LumoUtility.Padding.Vertical.NONE,
                LumoUtility.Padding.Horizontal.MEDIUM);
        header.getThemeList().set("dark",true);

        addToNavbar(header);

    }

    private void logout() {
        logoutButton.addClickListener(event -> {
            logoutButton.getUI().ifPresent((ui -> {
                ui.navigate("login");
            }));
        });
    }

    private void createDrawer() {
        addToDrawer(new VerticalLayout(
                new RouterLink("Клиенты", ClientListView.class),
                new RouterLink("Сотрудники", EmployeeListView.class),
                new RouterLink("Карты", CardListView.class),
        new RouterLink("Компьютеры", ComputerListView.class)
        ));
    }
}
