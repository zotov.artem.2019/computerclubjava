package com.spring.vkr.view.cardsView;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.entites.CardType;
import com.spring.vkr.data.entites.Client;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class CardForm extends FormLayout {

    TextField validity = new TextField("Срок действия");
    ComboBox<Client> client = new ComboBox<>("Клиент");
    ComboBox<CardType> type = new ComboBox<>("Тип");
    DatePicker dateOfIssue = new DatePicker("Дата оформления");

    Button save = new Button("Сохранить");
    Button delete = new Button("Удалить");
    Button cancel = new Button("Отмена");

    Binder<Card> binder = new BeanValidationBinder<>(Card.class);
    DatePicker.DatePickerI18n singleFormatI18n = new DatePicker.DatePickerI18n();

    public CardForm(List<CardType> types, List<Client> clients) {
        initComboBox(types, clients);
        datePickerValidate();
        binder.bindInstanceFields(this);
        addClassName("card-form");
        add(
                client,
                type,
                dateOfIssue,
                validity,
                createButtonLayout());
    }

    private void initComboBox(List<CardType> types, List<Client> clients) {
        client.setItems(clients);
        client.setItemLabelGenerator(Client::getFullName);
        type.setItems(types);
        type.setItemLabelGenerator(CardType::getName);
    }


    private void datePickerValidate() {
        singleFormatI18n.setDateFormat("yyyy-MM-dd");
        dateOfIssue.setI18n(singleFormatI18n);
    }


    public void setCard(Card card) {
        binder.setBean(card);
    }

    private Component createButtonLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickShortcut(Key.ENTER);
        cancel.addClickShortcut(Key.ESCAPE);
        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new CardForm.DeleteEvent(this, binder.getBean())));
        cancel.addClickListener(event -> fireEvent(new CardForm.CloseEvent(this)));

        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, delete, cancel);
    }

    private void validateAndSave() {
        if (binder.isValid()) {
            fireEvent(new CardForm.SaveEvent(this, binder.getBean()));
        }
    }

    public static abstract class CardFormEvent extends ComponentEvent<CardForm> {
        private final Card card;

        protected CardFormEvent(CardForm source, Card card) {
            super(source, false);
            this.card = card;
        }

        public Card getCard() {
            return card;
        }
    }

    public static class SaveEvent extends CardForm.CardFormEvent {
        SaveEvent(CardForm source, Card card) {
            super(source, card);
        }
    }

    public static class DeleteEvent extends CardForm.CardFormEvent {
        DeleteEvent(CardForm source, Card card) {
            super(source, card);
        }

    }

    public static class CloseEvent extends CardForm.CardFormEvent {
        CloseEvent(CardForm source) {
            super(source, null);
        }

    }

    public Registration addDeleteListener(ComponentEventListener<CardForm.DeleteEvent> listener) {
        return addListener(CardForm.DeleteEvent.class, listener);
    }

    public Registration addSaveListener(ComponentEventListener<CardForm.SaveEvent> listener) {
        return addListener(CardForm.SaveEvent.class, listener);
    }

    public Registration addCloseListener(ComponentEventListener<CardForm.CloseEvent> listener) {
        return addListener(CardForm.CloseEvent.class, listener);
    }
}