package com.spring.vkr.view.cardsView;

import com.spring.vkr.data.entites.Card;
import com.spring.vkr.data.services.CardService;
import com.spring.vkr.data.services.CardTypeService;
import com.spring.vkr.data.services.ClientService;
import com.spring.vkr.view.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "cards", layout = MainLayout.class)
@PageTitle("Cards | Computer club CRM")
public class CardListView extends VerticalLayout {
    private final Grid<Card> grid = new Grid<>(Card.class);
    private final TextField filterText = new TextField();

    private final ClientService clientService;
    private final CardTypeService typeService;

    private final CardService cardService;

    CardForm form;

    public CardListView(ClientService clientService, CardTypeService typeService, CardService cardService) {
        this.clientService = clientService;
        this.typeService = typeService;
        this.cardService = cardService;
        addClassName("cards-list-view");
        setSizeFull();
        configureGrid();
        configureForm();
        add(getToolbar(), getContent());
        updateList();
        closeEditor();
    }

    private void closeEditor() {
        form.setCard(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    public void editCard(Card card) {
        if (card == null) {
            closeEditor();
        } else {
            form.setCard(card);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void addCard() {
        grid.asSingleSelect().clear();
        editCard(new Card());
    }


    private void updateList() {
        grid.setItems(cardService.findAllCards(filterText.getValue()));
    }

    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, form);
        content.setFlexGrow(2, grid);
        content.setFlexGrow(1, form);
        content.addClassName("content");
        content.setSizeFull();
        return content;

    }

    private void configureForm() {
        form = new CardForm(typeService.findAllCardTypes(),
                clientService.findAllClients());
        form.setWidth("25em");
        form.addSaveListener(this::saveCard);
        form.addDeleteListener(this::deleteCard);
        form.addCloseListener(e -> closeEditor());
    }

    private void saveCard(CardForm.SaveEvent event) {
        cardService.saveCard(event.getCard());
        updateList();
        closeEditor();
    }

    private void deleteCard(CardForm.DeleteEvent event) {
        cardService.deleteCard(event.getCard());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("cards-grid");
        grid.setSizeFull();
        grid.removeAllColumns();
        grid.addColumn("id").setHeader("ID");
        grid.addColumn("dateOfIssue").setHeader("Дата оформления");
        grid.addColumn("validity").setHeader("Срок действия");
        grid.addColumn(card -> card.getClient().getFullName()).setHeader("ФИО клиента");
        grid.addColumn(card -> card.getType().getName()).setHeader("Тип карты");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));
        grid.asSingleSelect().addValueChangeListener(event ->
                editCard(event.getValue()));
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Поиск");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(event -> {
            updateList();
        });

        Button addCardButton = new Button("Add card");
        addCardButton.addClickListener(click -> addCard());
        var toolbar = new HorizontalLayout(filterText, addCardButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

}
