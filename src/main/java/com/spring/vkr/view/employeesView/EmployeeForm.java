package com.spring.vkr.view.employeesView;

import com.spring.vkr.data.entites.Employee;
import com.spring.vkr.data.entites.Position;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class EmployeeForm extends FormLayout {
    public static final String PHONE_PATTERN = "[+][7]\\s[(][0-9][0-9][0-9][)]\\s[0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]";
    TextField surname = new TextField("Фамилия");
    TextField name = new TextField("Имя");
    TextField patronymic = new TextField("Отчество");
    ComboBox<Position> position = new ComboBox<>("Должность");
    TextField login = new TextField("Логин");
    TextField password = new TextField("Пароль");
    TextField passportSeries = new TextField("Серия паспорта");
    TextField passportId = new TextField("Номер паспорта");
    TextField address = new TextField("Адрес");
    TextField phone = new TextField("Телефон");
    DatePicker dateOfBirth = new DatePicker("Дата рождения");
    DatePicker hireDate = new DatePicker("Дата найма");

    Button save = new Button("Сохранить");
    Button delete = new Button("Удалить");
    Button cancel = new Button("Отмена");

    Binder<Employee> binder = new BeanValidationBinder<>(Employee.class);
    DatePicker.DatePickerI18n singleFormatI18n = new DatePicker.DatePickerI18n();


    public EmployeeForm(List<Position> positions) {
        position.setItems(positions);
        position.setItemLabelGenerator(Position::getName);
        passportValidate();
        phoneValidate();
        datePickerValidate();
        binder.bindInstanceFields(this);
        addClassName("employee-form");
        add(
                surname,
                name,
                patronymic,
                position,
                login,
                password,
                passportSeries,
                passportId,
                address,
                phone,
                dateOfBirth,
                hireDate,
                createButtonLayout());
    }

    private void passportValidate() {
        passportSeries.setMinLength(4);
        passportSeries.setMaxLength(4);
        passportSeries.setAllowedCharPattern("[0-9]");
        passportId.setMinLength(6);
        passportId.setMaxLength(6);
        passportId.setAllowedCharPattern("[0-9]");
    }

    private void datePickerValidate() {
        singleFormatI18n.setDateFormat("yyyy-MM-dd");
        dateOfBirth.setI18n(singleFormatI18n);
    }

    private void phoneValidate() {
        phone.setRequiredIndicatorVisible(true);
        phone.setPattern(PHONE_PATTERN);
        phone.setAllowedCharPattern("[0-9()+- ]");
        phone.setMinLength(18);
        phone.setMaxLength(18);
        phone.setHelperText("Формат: +7 (123) 456-78-90");
    }

    public void setEmployee(Employee employee) {
        binder.setBean(employee);
    }

    private Component createButtonLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickShortcut(Key.ENTER);
        cancel.addClickShortcut(Key.ESCAPE);
        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new EmployeeForm.DeleteEvent(this, binder.getBean())));
        cancel.addClickListener(event -> fireEvent(new EmployeeForm.CloseEvent(this)));

        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, delete, cancel);
    }

    private void validateAndSave() {
        if(binder.isValid()) {
            fireEvent(new EmployeeForm.SaveEvent(this, binder.getBean()));
        }
    }

    public static abstract class EmployeeFormEvent extends ComponentEvent<EmployeeForm> {
        private final Employee employee;

        protected EmployeeFormEvent(EmployeeForm source, Employee employee) {
            super(source, false);
            this.employee = employee;
        }

        public Employee getEmployee() {
            return employee;
        }
    }

    public static class SaveEvent extends EmployeeForm.EmployeeFormEvent {
        SaveEvent(EmployeeForm source, Employee employee) {
            super(source, employee);
        }
    }

    public static class DeleteEvent extends EmployeeForm.EmployeeFormEvent {
        DeleteEvent(EmployeeForm source, Employee employee) {
            super(source, employee);
        }

    }

    public static class CloseEvent extends EmployeeForm.EmployeeFormEvent {
        CloseEvent(EmployeeForm source) {
            super(source, null);
        }
    }

    public Registration addDeleteListener(ComponentEventListener<EmployeeForm.DeleteEvent> listener) {
        return addListener(EmployeeForm.DeleteEvent.class, listener);
    }

    public Registration addSaveListener(ComponentEventListener<EmployeeForm.SaveEvent> listener) {
        return addListener(EmployeeForm.SaveEvent.class, listener);
    }
    public Registration addCloseListener(ComponentEventListener<EmployeeForm.CloseEvent> listener) {
        return addListener(EmployeeForm.CloseEvent.class, listener);
    }
}
