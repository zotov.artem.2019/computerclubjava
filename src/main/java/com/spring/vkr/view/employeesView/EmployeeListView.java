package com.spring.vkr.view.employeesView;

import com.spring.vkr.data.entites.Employee;
import com.spring.vkr.data.services.EmployeeService;
import com.spring.vkr.data.services.PositionService;
import com.spring.vkr.view.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "employees", layout = MainLayout.class)
@PageTitle("Employees | Computer club CRM")
public class EmployeeListView extends VerticalLayout {
    private final Grid<Employee> grid = new Grid<>(Employee.class);
    private final TextField filterText = new TextField();

    private final EmployeeService service;
    private final PositionService positionService;

    EmployeeForm form;

    public EmployeeListView(EmployeeService service, PositionService positionService) {
        this.service = service;
        this.positionService = positionService;
        addClassName("employees-list-view");
        setSizeFull();
        configureGrid();
        configureForm();
        add(getToolbar(), getContent());
        updateList();
        closeEditor();
    }
    private void closeEditor() {
        form.setEmployee(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    public void editEmployee(Employee employee) {
        if (employee == null) {
            closeEditor();
        } else {
            form.setEmployee(employee);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void addEmployee() {
        grid.asSingleSelect().clear();
        editEmployee(new Employee());
    }


    private void updateList() {
        grid.setItems(service.findAllEmployees(filterText.getValue()));
    }

    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, form);
        content.setFlexGrow(2,grid);
        content.setFlexGrow(1, form);
        content.addClassName("content");
        content.setSizeFull();
        return content;

    }

    private void configureForm() {
        form = new EmployeeForm(positionService.findAllPositions());
        form.setWidth("25em");
        form.addSaveListener(this::saveEmployee);
        form.addDeleteListener(this::deleteEmployee);
        form.addCloseListener(e -> closeEditor());
    }

    private void saveEmployee(EmployeeForm.SaveEvent event) {
        service.saveEmployee(event.getEmployee());
        updateList();
        closeEditor();
    }

    private void deleteEmployee(EmployeeForm.DeleteEvent event) {
        service.deleteEmployee(event.getEmployee());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("employees-grid");
        grid.setSizeFull();
        grid.setColumns("id", "surname", "name", "patronymic",
                "login", "password", "passportSeries",
                "passportId", "address","phone", "dateOfBirth","hireDate");
        grid.addColumn(contact -> contact.getPosition().getName()).setHeader("Position");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));
        grid.asSingleSelect().addValueChangeListener(event ->
                editEmployee(event.getValue()));
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Поиск");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(event -> {updateList();});

        Button addEmployeeButton = new Button("Add employee");
        addEmployeeButton.addClickListener(click -> addEmployee());
        var toolbar = new HorizontalLayout(filterText, addEmployeeButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }
}
